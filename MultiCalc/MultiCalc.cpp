﻿#include <iostream>

double number();
double Level_1();
double Level_2();
double Level_3();

double Level_3() {
    char c;
    double res = 0;
    std::cin >> c;
    switch (c) {
    case '(':
        res = Level_1();
        std::cin >> c;
        if (c == ')') 
            return res;
        else std::cin.putback(c);
        break;
    case '[':
        res = Level_1();
        std::cin >> c;
        if (c == ']')
            return res;
        else std::cin.putback(c);
        break;
    default:
        std::cin.putback(c);
        return number();
    }
}

double Level_2() {
    char c;
    double res = Level_3();
    while (std::cin >> c) {
        switch (c) {
        case '*':
            res *= Level_3();
            break;
        case '/':
            res /= Level_3();
            break;
        case '^':
            res = pow(res, Level_3());
            break;
        default:
            std::cin.putback(c);
            return res;
        }
    }
}

double Level_1(){
    char c;
    double res = Level_2();
    while (std::cin >> c) {
        switch (c) {
        case '+':
            res += Level_2();
            break;
        case '-':
            res -= Level_2();
            break;
        default:
            if (c != '=')
                std::cin.putback(c);
            return res;
        }
    }
}

double number() {
    char c;
    double num = 0;
    bool minus = false;
    bool fraction = false;
    int fraction_count = 0;

    std::cin >> c;
    if (c == '-') 
        minus = true;
    else
        std::cin.putback(c);
    while (std::cin >> c) {
        if (c >= '0' && c <= '9') {
            num = num * 10 + int(c) - int('0');
            if (fraction) {
                fraction_count += 1;
            }
        }
        else if (c == '.') {
            fraction = true;
        }
        else {
            std::cin.putback(c);
            if (minus)
                num *= -1;
            if (fraction_count > 0)
                num /= pow(10, fraction_count);
            return num;
            break;
        }
    }
}

void print() {
    std::cout << "\t\tКалькулятор\n";
    std::cout << "Правила:\n";
    std::cout << "() - Скобки\n";
    std::cout << "[] - Модуль\n";
    std::cout << "^ - Возведение ва степень\n";
    std::cout << "+,-,/.* - Математические операции\n";
    std::cout << "Окончание ввода всегда заканчивается '=' !!!\n\n";
    std::cout << "Пример1: 100+10*(2+5)=\n";
    std::cout << "Пример2: 10.5+6.5*(12.42/2+4/2-10*(1-0.5))=\n";
    std::cout << "Пример3: 10.5+6.5*((12.42/2+4/2-10*(1-0.5))^2-1)=\n";
    std::cout << "Пример4: (10/2)^(10-8/2-1)=\n\n";
}

int main()
{
    setlocale(LC_ALL, "ru");
    print();
    while (true) {
        std::cout << "Введите выражение: ";
        double calc = Level_1();

        std::cout << "Результат: " << calc << std::endl;
    };
    system("pause");
    return 0;
}